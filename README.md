# Intermediate Python

Next steps in Python programming,
for researchers who have already learned the basics of the language.

Pages adapted from [Software Carpentry's][swc-about] [Programming with Python lesson](https://github.com/swcarpentry/python-novice-inflammation/).

## Maintainers

Lesson maintainers are Renato Alves and Toby Hodges.

## Authors
A list of contributors to the lesson can be found in [AUTHORS](AUTHORS).

We are also grateful for minor fixes from:

- Bruno Contreras

## License
Instructional material from this lesson is made available under the
[Creative Commons Attribution][cc-by-human] ([CC BY 4.0][cc-by-legal]) license. Except where
otherwise noted, example programs and software included as part of this lesson are made available
under the [MIT license][mit-license]. For more information, see [LICENSE.md](LICENSE.md).
The Covid-19 dataset was obtained from the [EU Open Data Portal][eu-odp],
where it was published under the
[Creative Commons Attribution][cc-by-human] ([CC BY 4.0][cc-by-legal]) license.

## About Software Carpentry

Software Carpentry is a volunteer project that teaches basic computing skills to researchers since
1998. More information about Software Carpentry can be found [here][swc-about].

## About The Carpentries

The Carpentries is a fiscally sponsored project of [Community Initiatives][community-initiatives], a
registered 501(c)3 non-profit organisation based in California, USA. We are a global community
teaching foundational computational and data science skills to researchers in academia, industry and
government. More information can be found [here][cp-about].

[eu-odp]: https://data.europa.eu/euodp/en/
[lesson-example]: https://carpentries.github.io/lesson-example
[anne_fouilloux]: https://github.com/annefou
[lauren_ko]: https://github.com/ldko
[maxim_belkin]: https://github.com/maxim-belkin
[mike_trizna]: https://github.com/MikeTrizna
[trevor_bekolay]: http://software-carpentry.org/team/#bekolay_trevor
[valentina_staneva]: http://software-carpentry.org/team/#staneva_valentina
[greg_wilson]: https://github.com/gvwilson
[swc_history]: https://software-carpentry.org/scf/history/
[best-practices]: http://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001745
[good-practices]: http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005510
[R]: https://github.com/swcarpentry/r-novice-inflammation
[MATLAB]: https://github.com/swcarpentry/matlab-novice-inflammation
[shields_release]: https://img.shields.io/github/release/swcarpentry/python-novice-inflammation.svg
[swc_py_releases]: https://github.com/swcarpentry/python-novice-inflammation/releases
[create_slack_svg]: https://img.shields.io/badge/Create_Slack_Account-The_Carpentries-071159.svg
[slack_heroku_invite]: https://swc-slack-invite.herokuapp.com
[slack_channel_status]: https://img.shields.io/badge/Slack_Channel-swc--py--inflammation-E01563.svg
[slack_channel_url]: https://swcarpentry.slack.com/messages/C9Y0L6MF0
[travis_svg]: https://travis-ci.org/swcarpentry/python-novice-inflammation.svg?branch=gh-pages
[travis_url]: https://travis-ci.org/swcarpentry/python-novice-inflammation
[episode01]: https://swcarpentry.github.io/python-novice-inflammation/01-intro/index.html
[episode02]: https://swcarpentry.github.io/python-novice-inflammation/02-numpy/index.html
[episode03]: https://swcarpentry.github.io/python-novice-inflammation/03-matplotlib/index.html
[episode04]: https://swcarpentry.github.io/python-novice-inflammation/04-loop/index.html
[episode05]: https://swcarpentry.github.io/python-novice-inflammation/05-lists/index.html
[episode06]: https://swcarpentry.github.io/python-novice-inflammation/06-files/index.html
[episode07]: https://swcarpentry.github.io/python-novice-inflammation/07-cond/index.html
[episode08]: https://swcarpentry.github.io/python-novice-inflammation/08-func/index.html
[episode09]: https://swcarpentry.github.io/python-novice-inflammation/09-errors/index.html
[episode10]: https://swcarpentry.github.io/python-novice-inflammation/10-defensive/index.html
[episode11]: https://swcarpentry.github.io/python-novice-inflammation/11-debugging/index.html
[episode12]: https://swcarpentry.github.io/python-novice-inflammation/12-cmdline/index.html
[community-initiatives]: https://communityin.org
[cp-about]: https://carpentries.org/about
[swc-about]: https://software-carpentry.org/about/
[mit-license]: https://opensource.org/licenses/mit-license.html
[cc-by-human]: https://creativecommons.org/licenses/by/4.0/
[cc-by-legal]: https://creativecommons.org/licenses/by/4.0/legalcode
