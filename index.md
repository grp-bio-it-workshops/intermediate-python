---
layout: lesson
root: .
permalink: index.html
---

This lesson is designed to guide researchers, already familiar with the basics
of Python, through the next steps required to use the langauge most
effectively.

After following the course,
learners will be able to:

- recognise all elements of Python syntax they may encounter in other people's code.
- employ many language features and standard functions to write sophisticated programs and modules.
- load and analyse tabular and numeric data.
- visualise their data and create publication-ready figures.
- write code that is readable, maintainable, and follows community style standards.

> ## Prerequisites
>
> To follow this lesson, you should already be able to complete the following tasks with Python:
> define a variable;
> convert values between types e.g. integer, float, and string;
> compare values and write `if`/`elif`/`else` conditions;
> write a function definition;
> import a module;
> add items to and access values from a list or dictionary;
> save and run your programs as scripts in the shell.
>
> All of the concepts mentioned above are taught in the introductory
> Python course material offered by [EMBL Bio-IT][bio-it-itpp] and
> [Software Carpentry][swc-python-gapminder].
>
> In addition, you will need to follow the [setup instructions](setup/) before
> you begin working through this material.
>
> The commands in this lesson pertain to **Python 3**.
{: .prereq}

### Getting Started
To get started, follow the directions on the "[Setup](setup/)" page to download data
and install a Python interpreter.

### Acknowledgments

- The template of these pages is adapted from [The Carpentries Lesson Template][styles].
- Sections of this course material use data obtained from the [EU Open Data Portal][eu-odp], licensed under [CC-BY 4.0][cc-by-legal].
  A copy of this data is included in the [source repository](https://git.embl.de/grp-bio-it-workshops/intermediate-python).
- The lockdown data used in [Working with Data](02-data/) was adapted from
[this Wikipedia page](https://en.wikipedia.org/wiki/COVID-19_pandemic_lockdowns#table-of-pandemic-lockdowns),
accessed on 2020-06-24.

{% include links.md %}
