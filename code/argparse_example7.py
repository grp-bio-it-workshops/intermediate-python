import argparse

parser=argparse.ArgumentParser()
parser.add_argument("input_file",
                    help="path to an input file for processing")
parser.add_argument("number1", type=int, help="the first number")
parser.add_argument("number2", type=int, help="the second number")
parser.add_argument("--output", "-o",
                    help="path to an output file (default: print to STDOUT)")
parser.add_argument("--loud", "-l", action="store_true",
                    help="print info about the input filepath in ALL CAPS")
args = parser.parse_args()

message = f"input_file provided was {args.input_file}"
if args.loud:
    message=message.upper()
print(message)

result = args.number1 + args.number2
if args.output:
    with open(args.output, "w") as outfh:
        outfh.write(f"{result}\n")
else:
    print(result)
