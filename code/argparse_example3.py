import argparse

parser=argparse.ArgumentParser()
parser.add_argument("input_file",
                    help="path to an input file for processing")
args = parser.parse_args()

print(f"input_file provided was {args.input_file}")
