import argparse

parser=argparse.ArgumentParser()
parser.add_argument("input_file",
                    help="path to an input file for processing")
parser.add_argument("number1", type=int, help="the first number")
parser.add_argument("number2", type=int, help="the second number")
parser.add_argument("--language", "-l",
                    choices=["en", "de", "es", "pt"], default="en",
                    help="language of logging messages")
parser.add_argument("--output", "-o",
                    help="path to an output file (default: print to STDOUT)")

args = parser.parse_args()

logging_messages = {
    "en": "input file:\t{}",
    "de": "Eingabedatei:\t{}",
    "es": "nombre de archivo de entrada:\t{}",
    "pt": "nome do arquivo de entrada:\t{}",
}

message = logging_messages[args.language].format(args.input_file)
print(message)

result = args.number1 + args.number2
if args.output:
    with open(args.output, "w") as outfh:
        outfh.write(f"{result}\n")
else:
    print(result)
