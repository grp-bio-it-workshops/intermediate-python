#!/usr/bin/env bash

set -euxo pipefail

docker build -t git.embl.de:4567/grp-bio-it-workshops/intermediate-python .

# If successful update the image
docker login git.embl.de:4567
docker push git.embl.de:4567/grp-bio-it-workshops/intermediate-python
